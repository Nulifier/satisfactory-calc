import { IState } from ".";
import { machines } from "../data/machines";
import { recipes, minerRecipesByPurity, hardDriveRecipes } from "../data/recipes";
import { items } from "../data/items";
import { IRecipe, NodePurity } from "../models";

export function isBeltEnabled(state: IState) {
	return (beltId: number) => !state.disabledBelts.includes(beltId);
}

export function isMachineEnabled(state: IState) {
	return (machineId: number) => !state.disabledMachines.includes(machineId);
}

export function isRecipeEnabled(state: IState) {
	// TODO This should handle the miner purities but that might be removed soon
	return (recipeId: number) => hardDriveRecipes.includes(recipeId) ? state.enabledRecipes.includes(recipeId) : true;
}

export function enabledMachines(state: IState) {
	return machines.filter((machine) => !state.disabledMachines.includes(machine.id));
}

export function enabledRecipes(state: IState) {
	const disabled: number[] = [];

	// Get all the invalid miner purity levels
	Object.keys(minerRecipesByPurity)
	.filter((key) => key !== state.nodePurity)
	.forEach((key: NodePurity) => {
		disabled.push(...minerRecipesByPurity[key]);
	});

	// Get all the disabled hard drive recipes
	hardDriveRecipes
	.filter((id) => !state.enabledRecipes.includes(id))
	.forEach((id) => {
		disabled.push(id);
	});

	return recipes.filter((recipe) => !(disabled.includes(recipe.id)));
}

export function itemsWithRecipes(state: IState) {
	const recipeOutputs = enabledRecipes(state).map((recipe) => recipe.output.itemId);
	return items.filter((item) => (recipeOutputs.includes(item.id)));
}
