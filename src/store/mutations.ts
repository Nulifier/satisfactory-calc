import { IState, defaultState } from ".";
import { IItemRate, NodePurity } from "../models";

export function enablePersistence(state: IState, enabled: boolean) {
	state.persistState = enabled;
}

export function resetState(state: IState) {
	state = Object.assign(state, defaultState());
}

export function addRequiredOutput(state: IState, newOutput: IItemRate ) {
	state.requiredOutputs.push(newOutput);
}

export function removeRequiredOutput(state: IState, outputIndex: number) {
	state.requiredOutputs.splice(outputIndex, 1);
}

export function changeRequiredOutputRate(state: IState, payload: { outputIndex: number, newRate: number }) {
	if (payload.outputIndex < state.requiredOutputs.length) {
		state.requiredOutputs[payload.outputIndex].rate = payload.newRate;
	}
}

export function enableBelt(state: IState, payload: {beltId: number, enabled: boolean}) {
	// If we're enabling the belt, make sure it was disabled
	if (payload.enabled && state.disabledBelts.includes(payload.beltId)) {
		// Remove the belt from the disabled belts list
		state.disabledBelts.splice(state.disabledBelts.indexOf(payload.beltId), 1);
	}
	// If we're disabling a belt, make sure it isn't already disabled
	else if ((!payload.enabled) && (!state.disabledBelts.includes(payload.beltId))) {
		// Add the belt to the disabled belts list
		state.disabledBelts.push(payload.beltId);
	}
}

export function enableMachine(state: IState, payload: {machineId: number, enabled: boolean}) {
	// If we're enabling the machine, make sure it was disabled
	if (payload.enabled && state.disabledMachines.includes(payload.machineId)) {
		// Remove the machine from the disabled machines list
		state.disabledMachines.splice(state.disabledMachines.indexOf(payload.machineId), 1);
	}
	// If we're disabling a machine, make sure it isn't already disabled
	else if ((!payload.enabled) && (!state.disabledMachines.includes(payload.machineId))) {
		// Add the machine to the disabled machines list
		state.disabledMachines.push(payload.machineId);
	}
}

export function enableRecipe(state: IState, payload: {recipeId: number, enabled: boolean}) {
	// If we're enabling the recipe and the list doesn't include it already
	if (payload.enabled && !state.enabledRecipes.includes(payload.recipeId)) {
		// Add it to the list of enabled recipes
		state.enabledRecipes.push(payload.recipeId);
	}
	// If we're disabling it and the list contains it
	else if ((!payload.enabled) && state.enabledRecipes.includes(payload.recipeId)) {
		// Remove it from the list
		state.enabledRecipes.splice(state.enabledRecipes.indexOf(payload.recipeId), 1);
	}
}

export function changeNodePurity(state: IState, newPurity: NodePurity) {
	state.nodePurity = newPurity;
}
