import Vue from "vue";
import { exception } from "vue-analytics";
import router from "./router";

export interface IErrorInfo {
	source?: string;
	lineNum?: number;
	columnNum?: number;
	info?: string;
}

function reportError(message: string, error?: Error, extraInfo?: IErrorInfo) {
	// tslint:disable-next-line:no-console
	console.error("Error", message);

	exception(message);

	throw error || new Error(message);
}

window.onerror = (message: string, source?: string, lineNum?: number, columnNum?: number, error?: Error) => {
	reportError(message, error, {
		source,
		lineNum,
		columnNum
	});
	return true;
};

window.onunhandledrejection = ({promise, reason}) => {
	// All promises should have catch functions so calling this is an error
	reportError("Missing promise catch handler");
	reportError(reason);
};

Vue.config.errorHandler = (err: Error, vm: Vue, info: string) => {
	reportError(err.message, err, {
		info
	});
};

router.onError((err: Error) => {
	reportError(err.message, err);
});
