import { keyBy } from "lodash";
import { IBelt } from "../models";

export const belts = [
	{
		id: 1,
		name: "Conveyor Belt",
		rate: 60,
		icon: "images/icons/conveyor-belt-mk1.png"
	},
	{
		id: 2,
		name: "Conveyor Belt MK2",
		rate: 120,
		icon: "images/icons/conveyor-belt-mk2.png"
	},
	{
		id: 3,
		name: "Conveyor Belt MK3",
		rate: 270,
		icon: "images/icons/conveyor-belt-mk3.png"
	}
] as IBelt[];

export const beltsById = keyBy(belts, "id");
export const beltsByName = keyBy(belts, "name");
