import { keyBy } from "lodash";
import { IMachine } from "../models";

export const machines = [
	{
		id: 1,
		name: "Hand",
		icon: "images/icons/hand.png",
		type: "harvest",
		powerUsage: 0,
		rateMultiplier: 1.0
	},
	{
		id: 2,
		name: "Miner MK1",
		icon: "images/icons/miner-mk1.png",
		type: "miner",
		powerUsage: 5,
		rateMultiplier: 1.0
	},
	{
		id: 3,
		name: "Miner MK2",
		icon: "images/icons/miner-mk2.png",
		type: "miner",
		powerUsage: 12,
		rateMultiplier: 2.0
	},
	/*{
		id: 4,
		name: "Miner MK3",
		icon: "images/icons/miner-mk3.png",
		type: "miner",
		powerUsage: 12,
		rateMultiplier: 2.0
	},*/
	{
		id: 5,
		name: "Smelter",
		icon: "images/icons/smelter.png",
		type: "smelter",
		powerUsage: 4,
		rateMultiplier: 1.0
	},
	/*{
		id: 6,
		name: "Smelter MK2",
		icon: "images/icons/smelter-mk2.png",
		type: "smelter",
		powerUsage: 4,
		rateMultiplier: 1.0
	},*/
	{
		id: 7,
		name: "Foundry",
		icon: "images/icons/foundry.png",
		type: "foundry",
		powerUsage: 16,
		rateMultiplier: 1.0
	},
	/*{
		id: 8,
		name: "Foundry MK2",
		icon: "images/icons/foundry-mk2.png",
		type: "foundry",
		powerUsage: 4,
		rateMultiplier: 1.0
	},*/
	{
		id: 9,
		name: "Constructor",
		icon: "images/icons/constructor.png",
		type: "constructor",
		powerUsage: 4,
		rateMultiplier: 1.0
	},
	/*{
		id: 10,
		name: "Constructor MK2",
		icon: "images/icons/constructor-mk2.png",
		type: "constructor",
		powerUsage: 16,
		rateMultiplier: 1.0
	},*/
	{
		id: 11,
		name: "Assembler",
		icon: "images/icons/assembler.png",
		type: "assembler",
		powerUsage: 4,
		rateMultiplier: 1.0
	},
	/*{
		id: 12,
		name: "Assembler MK2",
		icon: "images/icons/assembler-mk2.png",
		type: "assembler",
		powerUsage: 16,
		rateMultiplier: 1.0
	},*/
	{
		id: 13,
		name: "Manufacturer",
		icon: "images/icons/manufacturer.png",
		type: "manufacturer",
		powerUsage: 30,
		rateMultiplier: 1.0
	},
	/*{
		id: 14,
		name: "Manufacturer MK2",
		icon: "images/icons/manufacturer-mk2.png",
		type: "manufacturer",
		powerUsage: 16,
		rateMultiplier: 1.0
	},*/
	{
		id: 15,
		name: "Oil Pump",
		icon: "images/icons/oil-pump.png",
		type: "pump",
		powerUsage: 20,
		rateMultiplier: 1.0
	},
	{
		id: 16,
		name: "Oil Refinery",
		icon: "images/icons/oil-refinery.png",
		type: "refinery",
		powerUsage: 25,
		rateMultiplier: 1.0
	},
	/*{
		id: 17,
		name: "Hadron Collider",
		icon: "images/icons/hadron-collider.png",
		type: "collider",
		powerUsage: 16,
		rateMultiplier: 1.0
	},*/
	/*{
		id: 18,
		name: "Quantum Encoder",
		icon: "images/icons/quantum-encoder.png",
		type: "encoder",
		powerUsage: 16,
		rateMultiplier: 1.0
	}*/
] as IMachine[];

export const machinesById = keyBy(machines, (machine) => machine.id);
export const machinesByName = keyBy(machines, (machine) => machine.name);
