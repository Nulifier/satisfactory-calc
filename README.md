## Satisfactory Calculator

A calculator for optimal ratios in the game Satisfactory by Coffee Stain Studios. Includes a graph viewer to visualize the layout of your factory. Helps you be efficient!

The application is available [here](https://nulifier.gitlab.io/satisfactory-calc/)

I'm open to any comments or suggestions and if there is any missing or incorrect info.
