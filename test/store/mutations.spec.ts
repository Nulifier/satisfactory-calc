import { expect } from "chai";
import * as mutations from "../../src/store/mutations";
import * as getters from "../../src/store/getters";
import { IState, defaultState } from "../../src/store";
import { belts } from "../../src/data/belts";
import { machines } from "../../src/data/machines";

describe("store/mutations", () => {
	let state: IState;

	beforeEach("Setup State", () => {
		// Reset the state to a fresh copy before each test
		state = defaultState();
	});

	describe("enablePersistence", () => {
		it("should set the state to false", () => {
			mutations.enablePersistence(state, false);
			expect(state.persistState).to.equal(false);
		});

		it("should set the state to true", () => {
			// Disable it first
			mutations.enablePersistence(state, false);
			mutations.enablePersistence(state, true);
			expect(state.persistState).to.equal(true);
		});
	});

	describe("resetState", () => {
		it("should properly reset the state", () => {
			mutations.enablePersistence(state, false);
			mutations.resetState(state);
			expect(state.persistState).to.equal(true);
		});
	});

	describe("addRequiredOutput", () => {
		it("should add an output to the list of outputs", () => {
			const numOutputs = state.requiredOutputs.length;
			mutations.addRequiredOutput(state, {
				itemId: 1,
				rate: 60
			});
			expect(state.requiredOutputs.length).to.equal(numOutputs + 1);
			expect(state.requiredOutputs[numOutputs].itemId).to.equal(1);
			expect(state.requiredOutputs[numOutputs].rate).to.equal(60);
		});
	});

	describe("removeRequiredOutput", () => {
		it("should remove an output", () => {
			const numOutputs = state.requiredOutputs.length;
			mutations.addRequiredOutput(state, {
				itemId: 1,
				rate: 37
			});
			expect(state.requiredOutputs.length).to.equal(numOutputs + 1);
			mutations.removeRequiredOutput(state, numOutputs);
			expect(state.requiredOutputs.length).to.equal(numOutputs);
			if (numOutputs > 0) {
				expect(state.requiredOutputs[state.requiredOutputs.length - 1].rate).to.not.equal(37);
			}
		});
	});

	describe("changeRequiredOutputRate", () => {
		it("should change the rate", () => {
			const outputIndex = state.requiredOutputs.length;
			mutations.addRequiredOutput(state, {
				itemId: 1,
				rate: 60
			});
			expect(state.requiredOutputs[outputIndex].rate).to.equal(60);
			mutations.changeRequiredOutputRate(state, { outputIndex, newRate: 37 });
			expect(state.requiredOutputs[outputIndex].rate).to.equal(37);
		});
	});

	describe("enableBelt", () => {
		it("should disable a belt type", () => {
			const isEnabled = getters.isBeltEnabled(state);
			for (const beltId of belts.map((belt) => belt.id)) {
				expect(isEnabled(beltId)).to.equal(true);
				mutations.enableBelt(state, { beltId, enabled: false });
				expect(isEnabled(beltId)).to.equal(false);
			}
		});

		it("should enable a belt type", () => {
			const isEnabled = getters.isBeltEnabled(state);
			for (const beltId of belts.map((belt) => belt.id)) {
				expect(isEnabled(beltId)).to.equal(true);
				mutations.enableBelt(state, { beltId, enabled: false });
				expect(isEnabled(beltId)).to.equal(false);
				mutations.enableBelt(state, { beltId, enabled: true });
				expect(isEnabled(beltId)).to.equal(true);
			}
		});
	});

	describe("enableMachine", () => {
		it("should disable a machine", () => {
			const isEnabled = getters.isMachineEnabled(state);
			for (const machineId of machines.map((machine) => machine.id)) {
				expect(isEnabled(machineId)).to.equal(true);
				mutations.enableMachine(state, { machineId, enabled: false });
				expect(isEnabled(machineId)).to.equal(false);
			}
		});

		it("should enable a machine", () => {
			const isEnabled = getters.isMachineEnabled(state);
			for (const machineId of machines.map((machine) => machine.id)) {
				expect(isEnabled(machineId)).to.equal(true);
				mutations.enableMachine(state, { machineId, enabled: false });
				expect(isEnabled(machineId)).to.equal(false);
				mutations.enableMachine(state, { machineId, enabled: true });
				expect(isEnabled(machineId)).to.equal(true);
			}
		});
	});
});
