import { expect } from "chai";
import { recipes, recipesById } from "../../src/data/recipes";
import { items } from "../../src/data/items";
import { RECIPE_TYPES } from "../constants";

describe("data/recipes", () => {
	it("should export a recipes array", () => {
		expect(recipes).to.be.an.instanceOf(Array);
		expect(recipes.length).to.greaterThan(0);
	});

	it("should have the required fields", () => {
		for (const recipe of recipes) {
			expect(recipe).to.have.property("id").that.is.a("number");
			expect(recipe).to.have.property("type").that.is.oneOf(RECIPE_TYPES);
			expect(recipe).to.have.property("baseRate").that.is.a("number");
			expect(recipe).to.have.property("output").that.is.an("object");
			expect(recipe.output).to.have.property("itemId").that.is.a("number");
			expect(recipe.output).to.have.property("quantity").that.is.a("number");
			expect(recipe).to.have.property("inputs").that.is.an("array");
			for (const input of recipe.inputs) {
				expect(input).to.have.property("itemId").that.is.a("number");
				expect(input).to.have.property("quantity").that.is.a("number");
			}
		}
	});

	it("should not have duplicate ids", () => {
		const usedIds: number[] = [];
		for (const recipe of recipes) {
			expect(recipe.id).to.not.be.oneOf(usedIds);
			usedIds.push(recipe.id);
		}
	});

	it("should export recipesById", () => {
		for (const recipe of recipes) {
			expect(recipesById[recipe.id]).to.equal(recipe);
		}
	});

	it("should use valid item ids", () => {
		const itemIds = items.map((item) => item.id);
		for (const recipe of recipes) {
			expect(recipe.output.itemId).to.be.oneOf(itemIds);
			for (const input of recipe.inputs) {
				expect(input.itemId).to.be.oneOf(itemIds);
			}
		}
	});
});
