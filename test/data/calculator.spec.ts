import { expect } from "chai";
import { findBestRecipe, findBestMachine, calculateSteps, calculateMachinesNeeded } from "../../src/data/calculator";
import { itemsByName } from "../../src/data/items";
import { machines, machinesByName } from "../../src/data/machines";
import { recipes, recipesById } from "../../src/data/recipes";

describe("data/calculator", () => {
	describe("findBestRecipe", () => {
		it("should exist", () => {
			expect(findBestRecipe).to.be.a("function");
		});

		it("should find a recipe", () => {
			const ironOre = itemsByName["Iron Ore"];
			const recipe = findBestRecipe(ironOre.id, recipes);
			expect(recipe).to.include.keys("inputs", "output");
		});

		it("should find the best recipe", () => {
			const ironOre = itemsByName["Iron Ore"];
			const recipe = findBestRecipe(ironOre.id, recipes);
			// There is a harvest recipe for iron ore that has a baseRate of 0
			expect(recipe.type).to.not.equal("harvest");
		});

		it("should throw when it can't find a recipe", () => {
			expect(() => findBestRecipe(-1, recipes)).to.throw();
		});
	});

	describe("findBestMachine", () => {
		it("should exist", () => {
			expect(findBestMachine).to.be.a("function");
		});

		it("should find a machine", () => {
			const machine = findBestMachine("miner", machines);
			expect(machine).to.include.keys("name", "powerUsage", "rateMultiplier");
		});

		it("should find the best machine", () => {
			const enabled = [
				machinesByName["Miner MK1"],
				machinesByName["Miner MK2"]
			];
			const machine = findBestMachine("miner", enabled);
			expect(machine.id).to.equal(enabled[1].id);
		});

		it("should only use enabled machines", () => {
			const enabled = [
				machinesByName["Miner MK1"]
			];
			const machine = findBestMachine("miner", enabled);
			expect(machine.id).to.equal(enabled[0].id);
		});

		it("should throw when it can't find a machine", () => {
			expect(() => findBestMachine("miner", [])).to.throw();
		});
	});

	describe("calculateMachinesNeeded", () => {
		it("should exist", () => {
			expect(calculateMachinesNeeded).to.be.a("function");
		});

		it("should work with a rateMultiplier of 1.0", () => {
			expect(calculateMachinesNeeded(60, 30, 1.0, 1)).to.equal(2);
		});

		it("should work with a rateMultiplier of 2.0", () => {
			expect(calculateMachinesNeeded(60, 30, 2.0, 1)).to.equal(1);
		});
	});

	describe("calculateSteps", () => {
		it("should exist", () => {
			expect(calculateSteps).to.be.a("function");
		});

		it("should work for iron ingots", () => {
			const ironIngot = itemsByName["Iron Ingot"];
			const enabledMachines = [
				machinesByName["Miner MK1"],
				machinesByName["Smelter"]
			];
			const enabledRecipes = [
				recipesById[9],
				recipesById[13]
			];

			const steps = calculateSteps([{ itemId: ironIngot.id, rate: 90 }], { enabledMachines, enabledRecipes });

			expect(steps).to.be.an("array");
			expect(steps.length).to.equal(1);

			const smelterStep = steps[0];
			expect(smelterStep).to.have.all.keys("recipeId", "machineId", "quantity", "required", "level");
			// The smelter recipes have a baseRate of 30/m with a rateMultiplier of 1.0 for the base smelter
			expect(smelterStep.quantity).to.equal(3);
			expect(smelterStep.level).to.equal(0);

			expect(smelterStep.required).to.be.an("array").with.length(1);
			const minerStep = smelterStep.required![0];
			expect(minerStep).to.have.all.keys("recipeId", "machineId", "quantity", "required", "level");
			// The smelter recipe is 1:1 so we should need 90 iron ore to match our 90 iron plates
			// 90 / (baseRate of 60) * (rateMultiplier of 1) = 1.5 machine
			expect(minerStep.quantity).to.equal(0.75);
		});
	});
});
